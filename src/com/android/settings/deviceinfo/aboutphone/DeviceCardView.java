package com.android.settings.deviceinfo.aboutphone;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.settings.R;
import com.android.settings.Utils;

public class DeviceCardView extends AboutBaseCard {
    private DeviceNamePreferenceHost mHost;
    private TypedArray mAttrs;
    private LinearLayout mLayout;
    private TextView mCardTitle;
    private TextView mCardSummary;
    private String mDeviceName;

    public DeviceCardView(@NonNull Context context) {
        super(context);
        init(context, null, 0);
    }

    public DeviceCardView(@NonNull Context context, @NonNull AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public DeviceCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    protected void init(Context context, AttributeSet attrs, int defStyleAttr) {
        super.init(context);
        mAttrs = context.obtainStyledAttributes(attrs, R.styleable.DeviceCardView, defStyleAttr, 0);
        mLayout = new LinearLayout(context);
        mLayout.setOrientation(LinearLayout.VERTICAL);
        mLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        mCardTitle = new TextView(context);
        mCardTitle.setText(mAttrs.getString(R.styleable.DeviceCardView_extra_title));
        mCardTitle.setPadding(0,0,0,24);
        mCardTitle.setTextSize(18);
        mCardTitle.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mCardSummary = new TextView(context);
        mDeviceName = Settings.Global.getString(context.getContentResolver(), Settings.Global.DEVICE_NAME);
        if (mDeviceName == null) {
            mDeviceName = Build.MODEL;
        }
        mCardSummary.setText(mDeviceName);
        mCardSummary.setTextColor(Utils.getColorAttrDefaultColor(context, android.R.attr.textColorSecondary));
        mCardSummary.setTextSize(14);
        mCardSummary.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mLayout.addView(mCardTitle);
        mLayout.addView(mCardSummary);
        mLayout.setBackgroundColor(getResources().getColor(R.color.search_bar_background));
        layout.addView(mLayout);
        mAttrs.recycle();
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeviceName = Settings.Global.getString(context.getContentResolver(), Settings.Global.DEVICE_NAME);
                if (mDeviceName == null) {
                    mDeviceName = Build.MODEL;
                }
                Builder mAlert = new Builder(mContext);
                EditText mEditText = new EditText(mContext);
                mEditText.setText(mDeviceName);
                mAlert.setTitle(mContext.getString(R.string.my_device_info_device_name_preference_title));
                mAlert.setView(mEditText);
                mAlert.setPositiveButton(com.android.internal.R.string.ok, null);
                mAlert.setNegativeButton(com.android.internal.R.string.cancel, null);
                mAlert.create();
                mAlert.show();
            }
        });
    }

    public void setHost(DeviceNamePreferenceHost host) {
        mHost = host;
    }

    public void setDeviceName(String deviceName) {
        mCardSummary.setText(deviceName);
    }

    public void setDeviceName(String deviceName, boolean validator) {
        if (validator) {
            setDeviceName(deviceName);
            setSettingsGlobalDeviceName(deviceName);
        }
    }

    public void setSettingsGlobalDeviceName(String deviceName) {
        Settings.Global.putString(mContext.getContentResolver(), Settings.Global.DEVICE_NAME, deviceName);
    }

    public interface DeviceNamePreferenceHost {
        void showDeviceNameWarningDialog(String deviceName);
    }
}
